package aurozen.client.registration;

import java.security.SecureRandom;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import aurozen.client.response.ResponseData;
import aurozen.client.response.ResponseList;

@Service
public class RegistrationService {
	
	@Autowired
	private EmployeeRepository employeeRepository;

	public ResponseList getEmployees() {
		ResponseList response = new ResponseList();
		
		List<Employees> list = employeeRepository.findAll();
		
		if(!list.isEmpty()) {

			response.setMessage("Success!");
			response.setResponse(true);
			response.setList(list);
			
		}else {

			response.setMessage("No Data Found!");
			response.setResponse(false);
		}
		
		return response;
	}
		
	@Transactional
	public ResponseData setEmployee(Employees employees, BindingResult result) {
		
		ResponseData response = new ResponseData();
		
		if(!result.hasErrors()) {
			
			employees.setEmpPass(generateRandomPassword(6));
			System.out.println(employees.getEmpId() +"22 :");
			employees = employeeRepository.save(employees);
			System.out.println(employees.getEmpId() +"23 :");
			
			if(employees != null) {
				response.setMessage("Successfully Created!");
				response.setResponse(true);
			}
		}else {
			System.out.println(result.getAllErrors());
			List<FieldError> errors = result.getFieldErrors();
			for (FieldError objectError : errors) {
				System.out.println("@" + objectError.getField().toUpperCase() + ":" + objectError.getDefaultMessage());
			}
			response.setMessage("Error With Your Data!");
			response.setResponse(false);
		}
		return response;
	}
	
	private String generateRandomPassword(int len)
	{
		// ASCII range - alphanumeric (0-9, a-z, A-Z)
		final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		SecureRandom random = new SecureRandom();
		StringBuilder sb = new StringBuilder();

		// each iteration of loop choose a character randomly from the given ASCII range
		// and append it to StringBuilder instance

		for (int i = 0; i < len; i++) {
			int randomIndex = random.nextInt(chars.length());
			sb.append(chars.charAt(randomIndex));
		}

		return sb.toString();
	}
}
