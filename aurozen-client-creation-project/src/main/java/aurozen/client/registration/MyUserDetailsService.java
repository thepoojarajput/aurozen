package aurozen.client.registration;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService{

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Override
	public UserDetails loadUserByUsername(String contact) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Employees emp = employeeRepository.findByEmpContact(contact);
		
		return new User(emp.getEmpContact(), emp.getEmpPass(), new ArrayList<>());
	}

}
