package aurozen.client.registration;

import java.io.Serializable;
import java.util.Properties;

import javax.persistence.EntityManager;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.springframework.stereotype.Service;
@Service
public class EmployeesIdGenerator implements IdentifierGenerator{

	EntityManager em;
	
	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		// TODO Auto-generated method stub

		String prefix = "emp000";// 
		int i = session.createNativeQuery("Select * from Employees", Employees.class).getResultList().size();
		String incrementedValue = new Integer(++i).toString();
		String generatedId = prefix + incrementedValue;
		return generatedId;
	}

}
