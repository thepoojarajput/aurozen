package aurozen.client.registration;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import aurozen.client.AuthenticationRequest;
import aurozen.client.AuthenticationResponse;
import aurozen.client.response.ResponseData;
import aurozen.client.response.ResponseList;
import aurozen.client.util.JwtUtil;

@RestController
@RequestMapping("/api")
public class RegistrationController {
	
	@Autowired
	private RegistrationService service;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private MyUserDetailsService userDetailsService;
	
	@Autowired
	private JwtUtil jwtTokenUtil;
	
	@GetMapping("/employees")
	public ResponseList getEmployees() {
		
		return service.getEmployees();
	}
	
	@PostMapping("/employees/login")
	public ResponseEntity<?> isLogin(@RequestBody AuthenticationRequest request) throws Exception{
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(request.getContact(), request.getPass()));
		}catch(BadCredentialsException e) {
			throw new Exception("Incorrect Username or Passord", e);
		}
		
		final UserDetails userDetails = userDetailsService.loadUserByUsername(request.getContact());
		final String jwt = jwtTokenUtil.generateToken(userDetails);
		
		return ResponseEntity.ok(new AuthenticationResponse(jwt));
	}

	@PostMapping("/employees")
	public ResponseData setEmployee(@RequestBody@Valid Employees employees, BindingResult result) {
		
		return service.setEmployee(employees, result);
	}
	

}
