package aurozen.client;

public class AuthenticationRequest {

	
	private String contact;
	private String pass;
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public AuthenticationRequest(String contact, String pass) {
		super();
		this.contact = contact;
		this.pass = pass;
	}
	public AuthenticationRequest() {
	}
	
	
}
